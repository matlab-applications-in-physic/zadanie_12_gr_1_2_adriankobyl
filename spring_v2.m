%Matlab 2011a script
%work with vibrations symulation
%
%author: AdrianKobyl
%date: 07-01-2019
%version: beta

%dane
%r drutu=2mm
%r zwoju=2
%m=10g
%st=stala tlumienia=2 s
%Fmax=mg
%czestotliwosc=f

%szukane
%F->a->v->x ?->A

%przydatne?
%F=-kx
%Ep=k*x^2/2 (max dla x=A)
%

%A(t)=alfa0/((w0^2-w^2)^2+4*beta^2*w^2)^0.5*sin(w*t-arctg((2*beta*w)/(w0^2-w^2))
%alfa0=F0/m 
%F0=-gamma*v
%gamma=m/teta
%v=(m*g-k*x)/m
%beta=1/(2*teta)
%w=(w0^2+beta^2)^0.5
%w0=(k/m)^0.5
%k=k=(G*r^4)/(4*N*R^3)
%G=80 GPa
%N rozne <2,50>
    %wzor z f pobudzania
%F(t)=-k*x-gamma*v+Fw(t)
%Fw(t)=F0*sin(w*t)


r=0.002; %promien drutu w metrach
R=0.051; %promien sprezymy w metrach
m=0.01; %masa klocka w kg
teta=2; %w sekundach?
g=9.81;

%inne dane
G=80000000000; %Pa


X2=zeros (1,48); %wartosci N, w wykresie jako war x
Y2=zeros (48,5); %amplitudy
time=linspace (1,5,5);

for iter = 2:50
    N=iter;
    X2 (1,N-1)=N;
    k=(G*r^4)/(4*N*R^3);
    w0=(k/m)^0.5;
    beta=1/(2*teta);
    w=(w0^2+beta^2)^0.5;
    alfa0=g;
    for t= 1:5
        A=alfa0/((w0^2-w^2)^2+4*beta^2*w^2)^0.5*sin(w*t-atan((2*beta*w)/(w0^2-w^2)));
        Y2 (N-1,t)=A;
       
        
    end
    
 
    
    
    
end

%plot (X2(1,1),Y2(1,:))

plot (time, Y2)
title ('amplitudy dla N z przedzialu <2;50>');
xlabel ('czas [s]');
ylabel ('amplituda [m]');
grid on;






















